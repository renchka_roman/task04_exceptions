package com.epam.course.renchka.model;

import com.epam.course.renchka.bean.beverage.Beverage;
import com.epam.course.renchka.bean.ingredient.CoffeeGround;
import com.epam.course.renchka.bean.ingredient.Ingredient;
import com.epam.course.renchka.bean.ingredient.Milk;
import com.epam.course.renchka.bean.ingredient.Water;
import com.epam.course.renchka.exception.CoffeeMachineBrakeDownException;
import com.epam.course.renchka.exception.NoIngredientException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Domain {

  private static final int WATER_CONTAINER_VOLUME_IN_ML = 2000;
  private static final int COFFEE_GROUND_CONTAINER_VOLUME_IN_GRAMS = 500;
  private static final int MILK_CONTAINER_VOLUME_IN_ML = 1000;

  private static Random random = new Random();

  private Map<Integer, String> coffeeTypes = new HashMap<>();
  private Map<String, Ingredient> ingredientContainers = new HashMap<>();

  {
    ingredientContainers.put("Water", new Water(2000));
    ingredientContainers.put(
        "Coffee grounds", new CoffeeGround(500));
    ingredientContainers.put("Milk", new Milk(1000));
  }

  private Map<Integer, String> createCoffeeTypes() {
    coffeeTypes.put(1, "Americano");
    coffeeTypes.put(2, "Espresso");
    coffeeTypes.put(3, "Cappuccino");
    coffeeTypes.put(4, "Latte");
    return coffeeTypes;
  }

  public Map<Integer, String> getCoffeeTypes() {
    if (coffeeTypes.size() != 0) {
      return coffeeTypes;
    }
    return createCoffeeTypes();
  }

  public void fillInMachineIngredientContainer(String ingredientName) {
    Ingredient ingredient = ingredientContainers.get(ingredientName);
    switch (ingredientName) {
      case "Water":
        ingredient.setVolume(WATER_CONTAINER_VOLUME_IN_ML);
        break;
      case "Milk":
        ingredient.setVolume(MILK_CONTAINER_VOLUME_IN_ML);
        break;
      case "Coffee grounds":
        ingredient.setVolume(COFFEE_GROUND_CONTAINER_VOLUME_IN_GRAMS);
        break;
    }
  }

  public Beverage createBeverage(List<Ingredient> BeverageIngredient,
      Class<? extends Beverage> clazz) throws NoIngredientException {
    if (random.nextInt(1000) == 999) {
      throw new CoffeeMachineBrakeDownException(
          "Fatal error. Please contact your service centre");
    }
    for (Ingredient ingredient : BeverageIngredient) {
      Ingredient availableIngredientInContainer =
          ingredientContainers.get(ingredient.toString());
      if (ingredient.getVolume() > availableIngredientInContainer.getVolume()) {
        throw new NoIngredientException(ingredient.toString());
      }
      availableIngredientInContainer.setVolume(
          availableIngredientInContainer.getVolume() - ingredient.getVolume());
    }
    try (Beverage beverage = clazz.newInstance()) {
      TimeUnit.SECONDS.sleep(2);
      return beverage;
    } catch (Exception e) {
    }
    return null;
  }
}