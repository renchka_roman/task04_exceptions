package com.epam.course.renchka.model;

import com.epam.course.renchka.exception.NoIngredientException;
import com.epam.course.renchka.view.View;
import java.io.IOException;

public interface Model {

  void setDomain(Domain domain);

  void setView(View view);

  void turnOn() throws IOException;

  void prepareBeverage(int menuPosition) throws NoIngredientException;

  void fillInMachineIngredientContainer(
      String ingredientName) throws IOException;
}
