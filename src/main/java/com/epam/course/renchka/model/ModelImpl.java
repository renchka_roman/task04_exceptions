package com.epam.course.renchka.model;

import com.epam.course.renchka.bean.beverage.Americano;
import com.epam.course.renchka.bean.beverage.Beverage;
import com.epam.course.renchka.bean.beverage.Cappuccino;
import com.epam.course.renchka.bean.beverage.Espresso;
import com.epam.course.renchka.bean.beverage.Latte;
import com.epam.course.renchka.exception.BeverageException;
import com.epam.course.renchka.exception.NoIngredientException;
import com.epam.course.renchka.view.View;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ModelImpl implements Model {

  private Domain domain;
  private View view;

  public void setDomain(Domain domain) {
    this.domain = domain;
  }

  public void setView(View view) {
    this.view = view;
  }

  @Override
  public void turnOn() throws IOException {
    view.printMessage("Coffee Machine is starting....Please wait.");
    try {
      TimeUnit.SECONDS.sleep(2);
      view.showMenu(domain.getCoffeeTypes());
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public void prepareBeverage(
      int menuPositionNumber) throws NoIngredientException {
    view.printSeparate();
    view.printMessage("Preparing...");
    Beverage beverage = getBeverage(menuPositionNumber);
    if (beverage != null) {
      try {
        view.printSeparate();
        System.out.println("Take your " + beverage.toString());
        TimeUnit.SECONDS.sleep(1);
        view.printSeparate();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    } else {
      throw new BeverageException(
          "Something wrong. Please restart Coffee Machine.");
    }
  }

  @Override
  public void fillInMachineIngredientContainer(
      String ingredientName) throws IOException {
    domain.fillInMachineIngredientContainer(ingredientName);
    view.showMenu(domain.getCoffeeTypes());
  }

  private Beverage getBeverage(
      int menuPositionNumber) throws NoIngredientException {
    Beverage beverage = null;
    switch (menuPositionNumber) {
      case 1:
        beverage = domain.createBeverage(
            Americano.getIngredients(), Americano.class);
        break;
      case 2:
        beverage = domain.createBeverage(
            Espresso.getIngredients(), Espresso.class);
        break;
      case 3:
        beverage = domain.createBeverage(
            Cappuccino.getIngredients(), Cappuccino.class);
        break;
      case 4:
        beverage = domain.createBeverage(
            Latte.getIngredients(), Latte.class);
        break;
    }
    return beverage;
  }
}
