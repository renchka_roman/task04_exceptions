package com.epam.course.renchka.view;

import com.epam.course.renchka.controller.Controller;
import com.epam.course.renchka.exception.BeverageException;
import com.epam.course.renchka.exception.CoffeeMachineBrakeDownException;
import com.epam.course.renchka.exception.NoIngredientException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

public class ViewImpl implements View {

  private BufferedReader reader =
      new BufferedReader(new InputStreamReader(System.in));
  private Controller controller;

  public void setController(Controller controller) {
    this.controller = controller;
  }

  @Override
  public void showMenu(Map<Integer, String> beverageTypes) throws IOException {
    String choice = "";
    try {
      while (!choice.toUpperCase().equals("Q")) {
        printSeparate();
        System.out.println("Menu:");
        printSeparate();
        for (Map.Entry<Integer, String> pair : beverageTypes.entrySet()) {
          System.out.println(
              "Press <" + pair.getKey() + "> - " + pair.getValue());
        }
        System.out.println("Press <Q> - Turn off coffee machine.");
        printSeparate();
        try {
          choice = reader.readLine();
          if (!choice.toUpperCase().equals("Q")) {
            int positionNumber = Integer.parseInt(choice);
            controller.prepareBeverage(positionNumber);
          }
        } catch (NumberFormatException e) {
          System.out.println("Unsupported command. Try again.");
        }
      }
    } catch (NoIngredientException e) {
      showNoIngredientMenu(e.getMessage());
    } catch (BeverageException e) {
      printErrorMenu(e.getMessage(), "Restart Coffee Machine.");
    } catch (CoffeeMachineBrakeDownException e) {
      printErrorMenu(e.getMessage(), "Shutdown Coffee Machine.");
    }
  }

  @Override
  public void printMessage(String message) {
    System.out.println(message);
  }

  @Override
  public void printSeparate() {
    System.out.println("-------------------------------------");
  }

  private void showNoIngredientMenu(String ingredientName) throws IOException {
    int choice = 0;
    while (choice != 1 && choice != 2) {
      System.out.print("There is not enough " + ingredientName + ". ");
      System.out.println("Please fix the problem or quit.");
      System.out.println("Press <1> - Add " + ingredientName);
      System.out.println("Press <2> - Quit.");
      try {
        choice = Integer.parseInt(reader.readLine());
      } catch (NumberFormatException e) {
      }
    }
    controller.fillInMachineIngredientContainer(ingredientName);
  }

  private void printErrorMenu(
      String message, String action) throws IOException {
    String choice = "";
    while (choice.equals("")) {
      printSeparate();
      System.out.println(message);
      printSeparate();
      System.out.println("Press <Any key> - " + action);
      printSeparate();
      choice = reader.readLine();
    }
  }
}
