package com.epam.course.renchka.view;

import com.epam.course.renchka.controller.Controller;
import java.io.IOException;
import java.util.Map;

public interface View {

  void setController(Controller controller);

  void printMessage(String message);

  void showMenu(Map<Integer, String> coffeeTypes) throws IOException;

  void printSeparate();
}
