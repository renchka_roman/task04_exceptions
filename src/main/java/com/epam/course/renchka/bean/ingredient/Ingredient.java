package com.epam.course.renchka.bean.ingredient;

public interface Ingredient {

  int getVolume();

  void setVolume(int value);
}
