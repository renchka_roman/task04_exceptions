package com.epam.course.renchka.bean.ingredient;

public class CoffeeGround implements Ingredient {

  private int weightInGram;

  public CoffeeGround(int weightInGram) {
    this.weightInGram = weightInGram;
  }

  @Override
  public int getVolume() {
    return weightInGram;
  }

  @Override
  public void setVolume(int weightInGram) {
    this.weightInGram = weightInGram;
  }

  @Override
  public String toString() {
    return "Coffee grounds";
  }
}
