package com.epam.course.renchka.bean.ingredient;

public class Milk implements Ingredient {

  private int volumeInMl;

  public Milk(int volumeInMl) {
    this.volumeInMl = volumeInMl;
  }

  @Override
  public int getVolume() {
    return volumeInMl;
  }

  @Override
  public void setVolume(int volumeInMl) {
    this.volumeInMl = volumeInMl;
  }

  @Override
  public String toString() {
    return "Milk";
  }
}
