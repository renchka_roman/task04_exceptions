package com.epam.course.renchka.bean.beverage;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public abstract class Beverage implements AutoCloseable {

  private static DateTimeFormatter formatter =
      DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
  private PrintWriter writer;
  private static int count = 0;
  private LocalDateTime date;

  public Beverage() {
    try {
      writer =
          new PrintWriter(
              new BufferedWriter(
                  new FileWriter("log.txt", true)));
      date = LocalDateTime.now();
      writer.println(date.format(formatter) + " - Preparing " + this + " ...");
      count++;
    } catch (IOException e) {
      System.out.println("File didn't open.");
    }
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }

  @Override
  public void close() throws Exception {
    date = LocalDateTime.now();
    writer.println(date.format(formatter) + " - Finished.");
    if (count == 10) {
      throw new Exception();
    }
    if (writer != null) {
      writer.close();
    }
  }
}
