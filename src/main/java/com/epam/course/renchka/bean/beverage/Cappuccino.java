package com.epam.course.renchka.bean.beverage;

import com.epam.course.renchka.bean.ingredient.CoffeeGround;
import com.epam.course.renchka.bean.ingredient.Ingredient;
import com.epam.course.renchka.bean.ingredient.Milk;
import com.epam.course.renchka.bean.ingredient.Water;
import java.util.ArrayList;
import java.util.List;

public class Cappuccino extends Beverage {

  private static List<Ingredient> ingredients = new ArrayList<>();

  static {
    ingredients.add(new Water(110));
    ingredients.add(new CoffeeGround(10));
    ingredients.add(new Milk(85));
  }

  public static List<Ingredient> getIngredients() {
    return ingredients;
  }
}
