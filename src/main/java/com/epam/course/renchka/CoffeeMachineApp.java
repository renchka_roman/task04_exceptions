package com.epam.course.renchka;

import com.epam.course.renchka.controller.Controller;
import com.epam.course.renchka.controller.ControllerImpl;
import com.epam.course.renchka.model.Domain;
import com.epam.course.renchka.model.Model;
import com.epam.course.renchka.model.ModelImpl;
import com.epam.course.renchka.view.View;
import com.epam.course.renchka.view.ViewImpl;
import java.io.IOException;

public class CoffeeMachineApp {

  public static void main(String[] args) throws IOException {
    View view = new ViewImpl();
    Domain domain = new Domain();
    Model model = new ModelImpl();
    model.setDomain(domain);
    model.setView(view);
    Controller controller = new ControllerImpl();

    view.setController(controller);
    controller.setModel(model);
    controller.turnOn();
  }
}
