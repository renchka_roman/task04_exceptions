package com.epam.course.renchka.exception;

public class NoIngredientException extends Exception {

  public NoIngredientException() {
  }

  public NoIngredientException(String message) {
    super(message);
  }
}
