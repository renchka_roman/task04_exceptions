package com.epam.course.renchka.exception;

public class BeverageException extends RuntimeException {

  public BeverageException() {
  }

  public BeverageException(String message) {
    super(message);
  }
}
