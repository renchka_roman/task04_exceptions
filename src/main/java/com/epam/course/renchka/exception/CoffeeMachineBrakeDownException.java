package com.epam.course.renchka.exception;

public class CoffeeMachineBrakeDownException extends RuntimeException {

  public CoffeeMachineBrakeDownException() {
  }

  public CoffeeMachineBrakeDownException(String message) {
    super(message);
  }
}
