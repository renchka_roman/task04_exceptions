package com.epam.course.renchka.controller;

import com.epam.course.renchka.exception.NoIngredientException;
import com.epam.course.renchka.model.Model;
import java.io.IOException;

public interface Controller {

  void setModel(Model model);

  void turnOn() throws IOException;

  void prepareBeverage(int menuPositionNumber) throws NoIngredientException;

  void fillInMachineIngredientContainer(
      String ingredientName) throws IOException;
}
