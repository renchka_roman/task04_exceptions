package com.epam.course.renchka.controller;

import com.epam.course.renchka.exception.NoIngredientException;
import com.epam.course.renchka.model.Model;
import java.io.IOException;

public class ControllerImpl implements Controller {

  private Model model;

  public void setModel(Model model) {
    this.model = model;
  }

  @Override
  public void turnOn() throws IOException {
    model.turnOn();
  }

  public void prepareBeverage(
      int menuPositionNumber) throws NoIngredientException {
    model.prepareBeverage(menuPositionNumber);
  }

  @Override
  public void fillInMachineIngredientContainer(String ingredientName)
      throws IOException {
    model.fillInMachineIngredientContainer(ingredientName);
  }
}
